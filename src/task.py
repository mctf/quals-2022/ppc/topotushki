import os
import sys
from flag_combo import combo
from pydub import AudioSegment


def create_wav(input_file):
    folder = "sounds/"
    footstep_up = AudioSegment.from_wav(folder + "footsteps-up.wav")
    footstep_down = AudioSegment.from_wav(folder + "footsteps-down.wav")
    footstep_right = AudioSegment.from_wav(folder + "footsteps-right.wav")
    footstep_left = AudioSegment.from_wav(folder + "footsteps-left.wav")
    skip = AudioSegment.from_wav(folder + "meow.wav")
    output = AudioSegment.empty()

    flag = os.environ['FLAG']
    path = ""

    for i in flag:
        path += combo[i]

    for direction in path:
        if direction == "U":
            output += footstep_up
        elif direction == "D":
            output += footstep_down
        elif direction == "R":
            output += footstep_right
        elif direction == "L":
            output += footstep_left
        output += skip

    output.export(f"{input_file}.wav", format="wav")


if __name__ == '__main__':
    create_wav(sys.argv[1])
