## Topotushki
Было перехвачено секретное перемещение котенка-разведчика, он каким-то образом смог передать сообщение нашим противникам!

Как же он это сделал?

## Файлы
К таску требуется приложить файл `topotushki.wav`

## Writeups 
1. Слушаем файл `topotushki.wav`
2. Понимаем, что существует 4 части в аудиодорожке
3. Понимаем, что каждая часть отделена от другой звуком `MEOW`
4. Пишем программу (или ручками для сильных :) ) чертим линии по звуку топота
5. Выводим на экран линии и получаем строчку `p4w_g0_uldr`

## Flag
MCTF{p4w_g0_uldr} / mctf{p4w_g0_uldr}


## Сложность 
Medium
